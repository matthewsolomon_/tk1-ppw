from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login , logout, authenticate
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm, AuthenticationForm

# Login System
def register(request):
    if request.user.is_authenticated:
        return redirect("home:index")
    context = {
        "registering":RegistrationForm
    }

    if request.method == "POST":
        registering = RegistrationForm(request.POST)
        if registering.is_valid():
            registering.save()
            new_user_account = authenticate(
                username = registering.cleaned_data['username'],
                password = registering.cleaned_data['password1']
            )
            login(request, new_user_account)

            return redirect("home:index")
        elif registering.errors:
            context["error"] = "Masukkan data dengan benar!"
    return render(request, 'login/register.html', context)

def logging_in(request):
    context = {"logging_in": LoginForm}
    if request.user.is_authenticated:
        return redirect("home:index")

    if request.method == "POST":
        logging_in = LoginForm(data = request.POST)
        if logging_in.is_valid():
            user_account = logging_in.get_user()
            login(request, user_account)
            return redirect("home:index")
        elif logging_in.errors:
            context["error"] = "Username atau Password salah"
    return render(request, 'login/login.html', context)

def logging_out(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("home:index")