from django.shortcuts import render, redirect
from .models import Payment
from .forms import FormDonasi, FormKartu

def donasi(request):   
    if(request.method == "POST"):
        form1 = FormDonasi(request.POST)
        form2 = FormKartu(request.POST)
        if(form1.is_valid() & form2.is_valid()):
            data_form = Payment()
            data_form.Nama = form1.cleaned_data["Nama"]
            data_form.Amount = form1.cleaned_data["Amount"]
            data_form.Message = form1.cleaned_data["Message"]
            data_form.CardNumber = form2.cleaned_data["CardNumber"]
            data_form.ValideDate = form2.cleaned_data["ValideDate"]
            data_form.Code = form2.cleaned_data["Code"]
            data_form.save()    
        return redirect("/donasi/")
    else:
        data_form = Payment.objects.all()
        form_donasi = FormDonasi()
        form_kartu = FormKartu()
        form_dict = {
            'data_form' : data_form,
            'form_donasi' : form_donasi,
            'form_kartu' : form_kartu
        }
        return render(request, 'donasi.html', form_dict)