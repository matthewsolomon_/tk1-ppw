from django.test import TestCase, Client
from .models import Payment
from .forms import FormDonasi, FormKartu
from .apps import DonasiConfig
from django.apps import apps
from .views import donasi

# Create your tests here.

class UnitTestNews(TestCase):
    def test_url(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/donasi/')
        self.assertTemplateUsed(response, 'donasi.html')

    def test_form_POST(self):
        response_post = Client().post('/donasi/', {'Nama':"Nana"})
        self.assertEqual(response_post.status_code,302)

    def test_apps(self):
        self.assertEqual(DonasiConfig.name, 'donasi')
        self.assertEqual(apps.get_app_config('donasi').name, 'donasi')

    def test_isi_index(self):
        response = Client().get('/donasi/')
        kembalian = response.content.decode('utf8')
        self.assertIn("WE NEED YOUR HELP!", kembalian)

    def test_model_payment(self):
        Payment.objects.create(Nama = "Nur" ,
            Amount = 123 ,
            Message = "Shalom",
            CardNumber = 123,
            ValideDate = 123,
            Code = 123)
        counter = Payment.objects.all().count()
        self.assertEqual(counter, 1)