from django.db import models
from datetime import datetime, date
from django.utils import timezone

# Create your models here.
class Payment(models.Model):
    Nama = models.CharField(blank=True, max_length= 100)
    Amount = models.PositiveIntegerField(blank=False)
    Message = models.CharField(blank=True, max_length= 100)
    CardNumber = models.PositiveIntegerField(blank=False, null=True)
    ValideDate = models.PositiveIntegerField(blank=False)
    Code = models.IntegerField(blank=False)
    Tanggal = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return self.Nama
