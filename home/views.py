from django.shortcuts import render
from .forms import TipsTrickForm
from .models import TipsTrick

# Create your views here.
def index(request):
    return render(request, 'home/index.html')

def penggunaan_masker(request):
    return render(request,'home/penggunaan-masker.html')

def tips_trick(request):
    if request.method == 'POST':
        tips_form = TipsTrickForm(request.POST)
        if tips_form.is_valid():
            tips_trick = TipsTrick()
            tips_trick.name = tips_form.cleaned_data['name']
            tips_trick.message = tips_form.cleaned_data['message']
            tips_trick.save()
    form = TipsTrickForm()
    response = {'form':form}
    response['data'] = TipsTrick.objects.all()
    return render(request, 'home/tips-trick.html',response)
