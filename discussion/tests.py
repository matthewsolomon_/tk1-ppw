from django.test import TestCase, Client
from .forms import PostForm, NewCommentForm
from .models import Post, Comment

class DiscussionTest(TestCase):
    def test_text_exist(self):
        response = self.client.get('/discussion/')

        self.assertIn('Discussion', response.content.decode())
        self.assertIn('Create', response.content.decode())
        self.assertIn('Post', response.content.decode())

    def test_create_button(self):
        response = self.client.get('/discussion/')

        self.assertIn('Create new Post', response.content.decode())
    
    def test_url_index(self):
        resp = Client().get('/discussion/')
        self.assertEquals(200, resp.status_code)
    
    def test_url_create(self):
        resp = Client().get('/discussion/newpost/')
        self.assertEquals(200, resp.status_code)

    def test_template_index(self):
        response = Client().get('/discussion/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_template_create(self):
        response = Client().get('/discussion/newpost/')
        self.assertTemplateUsed(response, 'newpost.html')

    def test_post_form_valid(self):
        response = self.client.post('/discussion/newpost/', data={
            'title' : 'Cara Menghindari COVID',
            'author' : 'Jaenal Abidin',
            'excerpt' : 'Bagaimana caranya?',
            'content' : 'Gini',
        })
        self.assertTrue(Post.objects.filter(title="Cara Menghindari COVID").exists())